#!/usr/bin/env python
#
# initialize_multi_coverage.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import subprocess
import numpy as np


class Config0:
    def __init__(self):

        self.filename_refseq_common = "mock.fasta"

        # simreads config

        self.replicate_count = 1
        self.coverages = [25, 50]
        self.haplotype_frequencies = [0.25, 0.25, 0.25, 0.25]
        self.seed = 1

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config1:
    def __init__(self):

        self.filename_refseq_common = "mock.fasta"

        # simreads config

        self.replicate_count = 100
        self.coverages = [10, 20, 30, 40, 50]
        self.alpha = .2
        self.haplotype_count = 21

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config2:
    def __init__(self):

        self.filename_refseq_common = "mock.fasta"

        # simreads config

        self.replicate_count = 100
        self.coverages = [10*i for i in range(1,11)]
        self.alpha = .2
        self.haplotype_count = 21

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config3:
    def __init__(self):

        self.filename_refseq_common = "16S_filtered_random_200.fasta"

        # simreads config

        self.replicate_count = 100
        self.coverages = [10*i for i in range(1,21)]
        self.alpha = .2
        self.haplotype_count = 200

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    


class Config4:
    def __init__(self):

        self.filename_refseq_common = "16S_filtered_random_200.fasta"

        # simreads config

        self.replicate_count = 100
        self.coverages = [25*i for i in range(1,13)]
        self.alpha = .2
        self.haplotype_count = 200

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-4    




def draw_haplotype_frequencies(haplotype_count, alpha):
    p = np.random.mtrand.dirichlet(np.ones(haplotype_count) * alpha)
    return p


def write_config_files(config):

    home = os.environ["HOME"]

    index = 0

    for replicate in range(config.replicate_count):

        if "alpha" in dir(config):
            haplotype_frequencies = draw_haplotype_frequencies(config.haplotype_count, alpha = config.alpha)
        else:
            haplotype_frequencies = config.haplotype_frequencies

        for coverage in config.coverages:

            index += 1

            # refseq

            filename_refseq = str(index) + '.refseq.fasta'
            subprocess.call(['cp', config.filename_refseq_common, filename_refseq]) 

            # simreads_raw config

            with open(str(index) + ".simreads_raw.config", 'w') as f:
                print("filename_refseq", filename_refseq, file=f)
                print("haplotype_frequencies", end=" ", file=f)
                for frequency in haplotype_frequencies: print(frequency, end=" ", file=f)
                print(file=f)
                if "seed" in dir(config): 
                    print("seed", config.seed, file=f)
                else:
                    print("seed", random.getrandbits(32), file=f)
                print("filename_stem", index, file=f) # important for makefile: this must match filename
                print("coverage", coverage, file=f)

            # harp freq config

            with open(str(index) + ".harp.freq.config", "w") as f:
                print("em_iter =", config.em_iter, file=f)
                print("em_converge =", config.em_converge, file=f)
                print("em_min_freq_cutoff = ", config.em_min_freq_cutoff, file=f)


def main():
    configs = [Config0(), Config1(), Config2(), Config3(), Config4()]

    if len(sys.argv) != 2:
        print("Usage: initialize_coverage.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

