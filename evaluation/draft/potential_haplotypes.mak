#
# potential_haplotypes.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


REGION = $(shell awk '/region/ {print $$2}' config.simreads)
FILENAME_REFSEQ = $(shell awk '/filename_refseq/ {print $$2}' config.simreads)
FILENAME_SNPS = $(shell awk '/filename_snps/ {print $$2}' config.simreads)

filename_refseq = ~/data/refseq/dmel-all-chromosome-r5.34.fasta

#filename_refseq /Users/darren/data/refseq/dmel-all-chromosome-r5.34.fasta
#filename_snps /Users/darren/data/dgrp_snps/Variants_Sparse_2L.sample_swap_fixed.txt
#region 2L:15000001-15010000


numbers = 5 10 15 20
targets = $(addsuffix .freqs,$(addprefix blah., $(numbers)))


main: info $(targets)


info: config.simreads
	@echo REGION: $(REGION)
	@echo FILENAME_REFSEQ: $(FILENAME_REFSEQ)
	@echo filename_refseq: $(filename_refseq)
	@echo FILENAME_SNPS: $(FILENAME_SNPS)
	@echo


#diff_freq_stringmatch: test.stringmatch.freqs
#	diff good.stringmatch.freqs test.stringmatch.freqs
#
#test.stringmatch.freqs: reads.sorted.bam
#	harp freq_stringmatch -b reads.sorted.bam -r $(REGION) --refseq $(FILENAME_REFSEQ) --snps $(FILENAME_SNPS) --haplotype_filter 1-4 --stem test --freqs test.stringmatch.freqs
#
#diff_freq: test.freqs
#	diff good.freqs test.freqs
#
#test.freqs: test.hlk
#	harp freq -r $(REGION) --hlk test.hlk





# $* stem of implicit rule match
# $@ target
# $< first prerequisite

blah.%.freqs : test.hlk
	harp freq --hlk test.hlk -r 2L:15000001-15010000 --stem blah.$* --haplotype_filter 1-$*

test.hlk: reads.sorted.bam.bai
	harp like -b reads.sorted.bam --refseq $(FILENAME_REFSEQ) --snps $(FILENAME_SNPS) -r $(REGION) --stem test

reads.sorted.bam.bai: reads.sorted.bam
	samtools index reads.sorted.bam

reads.sorted.bam: reads.bam
	samtools sort reads.bam reads.sorted

reads.bam: reads.sam
	samtools view -S -b reads.sam > reads.bam

reads.sam: config.simreads
	simreads

config.simreads:
	simreads config > config.simreads ;\
	echo 'seed 420' >> config.simreads

clean:
	- rm -r *.output
	- rm reads* true.hapfreqs seed.txt config.simreads test.* blah*
