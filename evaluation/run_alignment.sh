#!/bin/bash
#
# run_alignment.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi


if [ "$1" == "" ]
then
    echo 'Usage: run_alignment.sh filename_reads.fastq'
    exit 1
fi


filename_reads=$1
id=$SGE_TASK_ID
stem="ref$id"
filename_fasta=$stem.fasta
filename_sai=$stem.sai
filename_sam=$stem.sam
filename_bam=$stem.bam


bwa index ${filename_fasta}
bwa aln -n 20 ${filename_fasta} ${filename_reads} > ${filename_sai}
bwa samse ${filename_fasta} ${filename_sai} ${filename_reads} > ${filename_sam}
samtools view -S -b ${filename_sam} > ${filename_bam}

