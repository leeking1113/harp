#!/usr/bin/env python
#
# process_runs.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import glob
import sys 
import os
import numpy as np
from math import *


def d2(f, g):
    epsilon = 1e-4
    if abs(sum(f) - 1) > epsilon or abs(sum(g)-1) > epsilon: return float('nan')

    f_copy = np.copy(f)
    g_copy = np.copy(g)
    if len(f_copy) < len(g_copy): f_copy.resize(len(g))
    if len(g_copy) < len(f_copy): g_copy.resize(len(f))
    return sum((f_copy-g_copy)**2)


def entropy(f):
    sum = 0.
    for fi in f:
        if fi > 0: sum += -fi * log(fi)
    return sum


class Run:
    def __init__(self, index):
        self.index = index
        self.parse_files()
        self.compute_stats()

    def compute_stats(self):

        self.d_actual_true = d2(self.f_actual, self.f_true)
        n = len(self.f_actual)
        self.d_actual_uniform = d2(self.f_actual, np.ones(n)/n)
        self.entropy_actual = entropy(self.f_actual)

        self.d_estimate_true = d2(self.f_estimate, self.f_true)
        self.d_estimate_actual = d2(self.f_estimate, self.f_actual)

        f_estimate_relative_0 = self.f_estimate[1:]/sum(self.f_estimate[1:]) # hard-coded haplotype_index_related = 0
        f_actual_relative_0 = self.f_actual[1:]/sum(self.f_actual[1:])
        self.d_estimate_actual_relative = d2(f_estimate_relative_0, f_actual_relative_0)

        self.d_estimate_likelihood_true = d2(self.f_estimate_likelihood, self.f_true)
        self.d_estimate_likelihood_actual = d2(self.f_estimate_likelihood, self.f_actual)

        self.d_estimate_stringmatch_true = d2(self.f_estimate_stringmatch, self.f_true)
        self.d_estimate_stringmatch_actual = d2(self.f_estimate_stringmatch, self.f_actual)

        self.d_estimate_hippo_actual = d2(self.f_estimate_hippo, self.f_actual)
        self.d_estimate_aeml_actual = d2(self.f_estimate_aeml, self.f_actual)


    def parse_files(self):

        index = str(self.index)

        self.read_length = 0
        self.error_rate = 0
        self.unknown_frequency = 0
        
        if os.path.isfile(index + ".simreads.config"):
            filename_simreads_config = index + ".simreads.config"
        elif os.path.isfile(index + ".simreads_raw.config"):
            filename_simreads_config = index + ".simreads_raw.config"
        else:
            raise Exception("[process_runs.py] simreads config file not found")

        for line in open(filename_simreads_config):
            tokens = line.split()
            if len(tokens) == 0: continue
            if tokens[0] == "error_rate":
                self.error_rate = float(tokens[1])
            elif tokens[0] == "coverage":
                self.coverage = float(tokens[1])
            elif tokens[0] == "read_length":
                self.read_length = int(tokens[1])
            elif tokens[0] == "unknown_frequency":
                self.unknown_frequency = float(tokens[1])
            elif tokens[0] == "unknown_related_haplotype_index":
                self.unknown_related_haplotype_index = int(tokens[1])

        self.potential_haplotype_count = 0
        self.em_random_start_count = 0
        try:
            for line in open(index + ".harp.freq.config"):
                tokens = line.split()
                if len(tokens) == 0: continue
                if tokens[0] == "haplotype_filter":
                    self.potential_haplotype_count = int(tokens[2].split('-')[-1]) # "1-10" -> 10
                elif tokens[0] == "em_random_start_count":
                    self.em_random_start_count = int(tokens[2])
        except:
            pass


        self.bqi = 0
        try:
            for line in open(index + ".harp.like.config"):
                tokens = line.split()
                if len(tokens) == 0: continue
                if tokens[0] == "bqi":
                    self.bqi = 1
        except:
            pass


        self.logl_min_zscore = -1000
        try:
            for line in open(index + ".harp.like_multi.config"):
                tokens = line.split()
                if len(tokens) == 0: continue
                if tokens[0] == "logl_min_zscore":
                    self.logl_min_zscore = float(tokens[2])
        except:
            pass


        self.actual_unknown_freq = 0.
        try:
            for line in open(index + ".actual.unknown_freq"):
                tokens = line.split()
                if len(tokens) == 0: continue
                self.actual_unknown_freq = float(tokens[0])
        except:
            pass


        self.f_estimate = self.parse_freqs(index + ".freqs")
        self.f_estimate_likelihood = self.parse_freqs(index + ".likelihood.freqs")
        self.f_estimate_stringmatch = self.parse_freqs(index + ".stringmatch.freqs")
        self.f_estimate_hippo = self.parse_freqs(index + ".hippo.freqs")
        self.f_estimate_aeml = self.parse_freqs(index + ".aeml.freqs")
        self.f_actual = self.parse_freqs(index + ".actual.freqs")
        self.f_true = self.parse_freqs(index + ".true.freqs")


    def parse_freqs(self, filename):
        try:
            f = open(filename)
        except:
            return np.zeros(len(self.f_estimate))
        tokens = f.readline().split()
        if "region_id" not in dir(self):
            self.region_id = tokens[0]
            self.region_begin = int(tokens[1])
            self.region_end = int(tokens[2])
        frequencies = np.array([float(x) for x in tokens[3:]])
        return np.array(frequencies)

    
    @classmethod
    def header(cls):
        return "index error_rate coverage read_length potential_haplotype_count em_random_start_count region region_width bqi d_actual_true d_actual_uniform actual_entropy d_estimate_true d_estimate_actual d_estimate_actual_relative d_estimate_likelihood_true d_estimate_likelihood_actual d_estimate_stringmatch_true d_estimate_stringmatch_actual d_estimate_hippo_actual d_estimate_aeml_actual unknown_frequency logl_min_zscore"

    def __str__(self):
        return str(self.index) + " " + \
               str(self.error_rate) + " " + \
               str(self.coverage) + " " + \
               str(self.read_length) + " " + \
               str(self.potential_haplotype_count) + " " + \
               str(self.em_random_start_count) + " " + \
               self.region_id + ":" + str(self.region_begin + 1) + "-" + str(self.region_end) + " " + \
               str(self.region_end - self.region_begin) + " " + \
               str(self.bqi) + " " + \
               str(self.d_actual_true) + " " + \
               str(self.d_actual_uniform) + " " + \
               str(self.entropy_actual) + " " + \
               str(self.d_estimate_true) + " " + \
               str(self.d_estimate_actual) + " " + \
               str(self.d_estimate_actual_relative) + " " + \
               str(self.d_estimate_likelihood_true) + " " + \
               str(self.d_estimate_likelihood_actual) + " " + \
               str(self.d_estimate_stringmatch_true) + " " + \
               str(self.d_estimate_stringmatch_actual) + " " + \
               str(self.d_estimate_hippo_actual) + " " + \
               str(self.d_estimate_aeml_actual) + " " + \
               str(self.unknown_frequency) + " " + \
               str(self.logl_min_zscore) + " " + \
               "" 


def main():
    run_count = len(glob.glob("*.simreads*config"))

    print(Run.header())
    
    #for i in [25*i for i in range(1,13)]: # hack for original run 25, 50, ...
    for i in range(1, run_count+1):
        try:
            run = Run(i)
            print(run)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()

