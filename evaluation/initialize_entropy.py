#!/usr/bin/env python
#
# initialize_entropy.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import numpy as np


class Config0:
    def __init__(self):

        # simreads config

        self.replicate_count = 2
        self.alphas = [.1, .5, 1, 5, 10]
        self.coverage = 200
        self.regions = ["2L:15000001-15100000"]
        self.error_rate = 0.066
        self.seed = 1

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-5


class Config1:
    def __init__(self):

        # simreads config

        self.replicate_count = 20
        self.alphas = [.005, .01, .02, .05, .1, .5, 1, 5, 10]
        self.coverage = 200
        self.regions = ["2L:15000001-15100000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-5


class Config2:
    def __init__(self):

        # simreads config

        self.replicate_count = 50
        self.alphas = [.005, .01, .015, .02, .05, .1, .2, .5, 1, 5, 10]
        self.coverage = 200
        self.regions = ["2L:15000001-15100000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-5


class Config3:
    def __init__(self):

        # simreads config

        self.replicate_count = 50
        self.alphas = [.005, .01, .015, .02, .05, .1, .2, .5, 1, 5, 10]
        self.coverage = 200
        self.regions = ["3L:8000001-8200000"]
        self.error_rate = 0.066

        # harp freq config

        self.em_iter = 200
        self.em_converge = 1e-8
        self.em_min_freq_cutoff = 1e-5



def arm(region):
    if region.startswith("X"): return "X"
    else: return region[:2]


def draw_haplotype_frequencies(alpha):
    haplotype_count = 162
    p = np.random.mtrand.dirichlet(np.ones(haplotype_count) * alpha)
    #print("blah:", alpha, np.sum(-p*np.log(p)))
    return p


def write_config_files(config):

    home = os.environ["HOME"]
    filename_refseq = home + "/data/refseq/dmel-all-chromosome-r5.34.fasta"

    index = 0
    for region in config.regions:
        filename_snps = home + "/data/dgrp_snps/Variants_Sparse_" + arm(region) + ".sample_swap_fixed.txt"

        for alpha in config.alphas:

            for replicate in range(config.replicate_count):
                
                index += 1

                haplotype_frequencies = draw_haplotype_frequencies(alpha)

                # simreads config

                filename = str(index) + ".simreads.config"
                f = open(filename, "w")
                print("filename_refseq", filename_refseq, file=f)
                print("filename_snps", filename_snps, file=f)
                print("region", region, file=f)
                print("haplotype_frequencies", end=" ", file=f)
                for frequency in haplotype_frequencies: print(frequency, end=" ", file=f)
                print(file=f)
                print("error_rate", config.error_rate, file=f)
                if "seed" in dir(config): 
                    print("seed", config.seed, file=f)
                else:
                    print("seed", random.getrandbits(32), file=f)
                print("filename_stem", index, file=f) # important for makefile: this must match filename
                print("coverage", config.coverage, file=f)
                f.close()

                # harp like config

                filename = str(index) + ".harp.like.config"
                f = open(filename, "w")
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                f.close()

                # harp freq config

                filename = str(index) + ".harp.freq.config"
                f = open(filename, "w")
                print("function = freq", file=f)
                print("refseq =", filename_refseq, file=f)
                print("snps =", filename_snps, file=f)
                print("region =", region, file=f)
                print("em_iter =", config.em_iter, file=f)
                print("em_converge =", config.em_converge, file=f)
                print("em_min_freq_cutoff = ", config.em_min_freq_cutoff, file=f)
                f.close()


def main():
    configs = [Config0(), Config1(), Config2(), Config3()]

    if len(sys.argv) != 2:
        print("Usage: initialize_entropy.py n")
        sys.exit(0)

    config_index = int(sys.argv[1]) 
    write_config_files(configs[config_index])
    

if __name__ == '__main__':
    main()

