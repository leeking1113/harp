#!/bin/bash
#
# map_reads.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


filename_reads=example2.reads.fastq
filestem="ref"

for i in 0 1 2 3
do
    filename_fasta=$filestem$i.fasta
    filename_sai=$filestem$i.sai
    filename_sam=$filestem$i.sam
    filename_bam=$filestem$i.bam

    bwa index $filename_fasta
    bwa aln -n 20 $filename_fasta $filename_reads > $filename_sai
    bwa samse $filename_fasta $filename_sai $filename_reads > $filename_sam
    samtools view -S -b $filename_sam > $filename_bam
done
