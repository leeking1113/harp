//
// harp_allele_counts.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <iomanip>
#include <iterator>
#include <cmath>
#include <numeric>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;


namespace {


struct Config
{
    string filename_freqs;
    string filename_refseq;
    string filename_snps;
    string region;

    Config(int argc, char* argv[])
    {
        const char* usage = "Usage: harp allele_counts file.freqs file_refseq file_snps region\n";

        if (argc != 6) throw runtime_error(usage);
        if (strcmp(argv[1], "allele_counts")) throw runtime_error("[harp_allele_counts] This isn't happening.");

        filename_freqs = argv[2];
        filename_refseq = argv[3];
        filename_snps = argv[4];
        region = argv[5];
    }
};


struct HaplotypeFrequencyEntry
{
    Region region;
    vector<double> f;
};


vector<HaplotypeFrequencyEntry> read_table(const string& filename, const string& chromosome_id)
{
    vector<HaplotypeFrequencyEntry> result;

    ifstream is(filename.c_str());
    while (is)
    {
        string buffer;
        getline(is, buffer);
        if (!is) break;

        result.push_back(HaplotypeFrequencyEntry());
        HaplotypeFrequencyEntry& entry = result.back();

        entry.region.id = chromosome_id;
        istringstream iss(buffer);
        iss >> entry.region.begin >> entry.region.end;
        copy(istream_iterator<double>(iss), istream_iterator<double>(), back_inserter(entry.f));
    }

    return result;
}


inline bool isACGT(char base)
{
    return (base=='A' || base=='C' || base=='G' || base=='T');
}


struct AlleleCounts
{
    enum Base {A, C, G, T};
    static map<char, size_t> base_index;

    void initialize_base_index()
    {
        base_index['A'] = A;
        base_index['C'] = C;
        base_index['G'] = G;
        base_index['T'] = T;
    }

    AlleleCounts(size_t _snp_position,
                 const vector<char>& _snp_alleles,
                 const vector<double>& _haplotype_frequencies,
                 size_t max_freq_index)
    :   snp_position(_snp_position), count_increased(4), count_decreased(4), count_start(4), 
        allele_ref('\0'), allele_max('\0')
    {
        if (base_index.empty()) initialize_base_index();

        assert(_snp_alleles.size() == _haplotype_frequencies.size() + 1); // _snp_alleles[0] is Ref

        allele_ref = _snp_alleles[0];
        allele_max = _snp_alleles[max_freq_index];
        if (!isACGT(allele_max)) allele_max = allele_ref;

        const double uniform = 1./_haplotype_frequencies.size();

        // for now, no sorting 
        for (size_t i=0; i<_haplotype_frequencies.size(); ++i)
        {
            char allele = _snp_alleles[i+1]; 
            if (!isACGT(allele)) allele = _snp_alleles[0]; // if ambiguous, then use reference base

            double freq = _haplotype_frequencies[i];

            ++count_start[base_index[allele]];

            if (freq > uniform)
                ++count_increased[base_index[allele]];
            else
                ++count_decreased[base_index[allele]];
        }

        // sanity check
        size_t sum1 = accumulate(count_increased.begin(), count_increased.end(), 0);
        sum1 = accumulate(count_decreased.begin(), count_decreased.end(), sum1);
        size_t sum2 = accumulate(count_start.begin(), count_start.end(), 0);
        assert(sum1 == _haplotype_frequencies.size() && sum2 == sum1);
    }

    size_t snp_position;
    vector<size_t> count_increased;
    vector<size_t> count_decreased;
    vector<size_t> count_start;
    char allele_ref;
    char allele_max; // allele of max-frequency haplotype

    static const char* header;
};


map<char, size_t> AlleleCounts::base_index;


const char* AlleleCounts::header = 
    "# 1_based_position allele_ref allele_max_freq_haplotype inc_A inc_C inc_G inc_T dec_A dec_C dec_G dec_T start_A start_C start_G start_T";


ostream& operator<<(ostream& os, const AlleleCounts& a)
{
    os << a.snp_position+1 << " " // 1-based position
       << a.allele_ref << " "
       << a.allele_max << " ";
    copy(a.count_increased.begin(), a.count_increased.end(), ostream_iterator<size_t>(os, " "));
    copy(a.count_decreased.begin(), a.count_decreased.end(), ostream_iterator<size_t>(os, " "));
    copy(a.count_start.begin(), a.count_start.end(), ostream_iterator<size_t>(os, " "));
    return os;
}


size_t max_freq_haplotype_index(const vector<double>& f)
{
    double max_freq = 0.;
    size_t result = -1ul;

    for (size_t i=0; i<f.size(); ++i)
    if (f[i] > max_freq)
    {
        max_freq = f[i];
        result = i;
    }

    return result;
}


void go(const Config& config)
{
    // read in haplotype frequencies

    Region region(config.region);
    vector<HaplotypeFrequencyEntry> haplotype_frequencies = read_table(config.filename_freqs, region.id);

    // print header line
    
    cout << AlleleCounts::header << endl;

    // iterate through the haplotype frequency windows

    for (vector<HaplotypeFrequencyEntry>::const_iterator it=haplotype_frequencies.begin(); it!=haplotype_frequencies.end(); ++it) 
    {
        // read in refseq/snp data for the intersection of the region and the window

        Region region_intersection = it->region & region;
        if (region_intersection.empty()) continue;

        const vector<double>& f = it->f;

        shared_ptr<HaplotypeReference> hapref(
            new HaplotypeReference(config.filename_refseq,
                                   config.filename_snps,
                                   region_intersection));

        // iterate through SNPs in the region

        HaplotypeReference::SNPTable::const_iterator it = hapref->snp_table.lower_bound(region_intersection.begin);
        HaplotypeReference::SNPTable::const_iterator end = hapref->snp_table.lower_bound(region_intersection.end);

        for (; it!=end; ++it)
        {
            size_t snp_position = it->first;
            const vector<char>& alleles = it->second;
            AlleleCounts a(snp_position, alleles, f, max_freq_haplotype_index(f));
            cout << a << endl;
        }
    }
}


} // namespace


int harp_allele_counts(int argc, char* argv[])
{
    Config config(argc, argv);
    go(config);
    return 0;
}


