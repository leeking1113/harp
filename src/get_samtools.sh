#!/bin/bash

VERSION=0.1.18

#curl -O -L http://sourceforge.net/projects/samtools/files/samtools/$VERSION/samtools-$VERSION.tar.bz2
wget http://sourceforge.net/projects/samtools/files/samtools/$VERSION/samtools-$VERSION.tar.bz2
tar xjvf samtools-$VERSION.tar.bz2
ln -s samtools-$VERSION samtools
cd samtools
make
