//
// HaplotypeReferenceMulti.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HAPLOTYPEREFERENCEMULTI_HPP_
#define _HAPLOTYPEREFERENCEMULTI_HPP_


#include <vector>
#include <string>
#include <iosfwd>
#include <tr1/memory>


class HaplotypeReferenceMulti
{
    public:

    HaplotypeReferenceMulti(const std::string& filename_fasta);
    HaplotypeReferenceMulti(const std::vector<std::string>& filenames_fasta);

    size_t haplotype_count() const;
    std::string id(size_t haplotype_index) const;
    size_t sequence_length(size_t haplotype_index) const;
    std::string full_sequence(size_t haplotype_index, size_t position_begin, size_t position_end) const;

    private:

    // simple implementation: store full sequences in memory

    struct Haplotype
    {
        std::string id;
        std::string sequence;

        void read_fasta_record(std::istream& is);
    };

    typedef std::tr1::shared_ptr<Haplotype> HaplotypePtr;

    std::vector<HaplotypePtr> haplotypes_;
};


#endif // _HAPLOTYPEREFERENCEMULTI_HPP_

