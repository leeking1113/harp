//
// PopulationSequenceMapperTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "PopulationSequenceMapper.hpp"
#include "Population_Organisms.hpp"
#include "unit.hpp"


using namespace std;


ostream* os_ = 0;


void test()
{
    if (os_) *os_ << "test()\n";

    Region region("madeup:1000-1040");

    HaplotypeReference hapref("test_files/refseq_A.fasta", 
                              "test_files/sparse_snps_madeup.txt", 
                              region);

    unit_assert(hapref.haplotype_count() == 5);

    if (os_)
    {
        for (size_t i=0; i<4; ++i)
        {
            string sequence = hapref.full_sequence(i, 1000, 1040); // madeup:1001-1040
            *os_ << i << " " << sequence << endl;
            const char* bases = "ACGT";
            for (size_t j=0; j<40; ++j)
                unit_assert(sequence[j] == bases[i]);
        }
    }

    HaplotypeChunks hapchunks_1a;
    hapchunks_1a.push_back(HaplotypeChunk(0, 0));
    hapchunks_1a.push_back(HaplotypeChunk(1010, 1));
    string sequence_1a_good = "AAAAAAAAAACCCCCCCCCCCCCCCCCCCCCCCCCCCCCC";
    Chromosome chromosome_1a(hapchunks_1a);
    Organism::Gamete gamete_1a;
    gamete_1a.push_back(chromosome_1a);

    HaplotypeChunks hapchunks_1b;
    hapchunks_1b.push_back(HaplotypeChunk(0, 1));
    hapchunks_1b.push_back(HaplotypeChunk(1030, 2));
    string sequence_1b_good = "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCGGGGGGGGGG";
    Chromosome chromosome_1b(hapchunks_1b);
    Organism::Gamete gamete_1b;
    gamete_1b.push_back(chromosome_1b);

    Organism organism_1(gamete_1a, gamete_1b);

    if (os_) *os_ << "organism_1:\n" << organism_1 << endl;

    HaplotypeChunks hapchunks_2a;
    hapchunks_2a.push_back(HaplotypeChunk(0, 3));
    hapchunks_2a.push_back(HaplotypeChunk(1010, 2));
    hapchunks_2a.push_back(HaplotypeChunk(1020, 1));
    string sequence_2a_good = "TTTTTTTTTTGGGGGGGGGGCCCCCCCCCCCCCCCCCCCC";
    Chromosome chromosome_2a(hapchunks_2a);
    Organism::Gamete gamete_2a;
    gamete_2a.push_back(chromosome_2a);

    HaplotypeChunks hapchunks_2b;
    hapchunks_2b.push_back(HaplotypeChunk(0, 2));
    hapchunks_2b.push_back(HaplotypeChunk(1020, 3));
    hapchunks_2b.push_back(HaplotypeChunk(1030, 0));
    string sequence_2b_good = "GGGGGGGGGGGGGGGGGGGGTTTTTTTTTTAAAAAAAAAA";
    Chromosome chromosome_2b(hapchunks_2b);
    Organism::Gamete gamete_2b;
    gamete_2b.push_back(chromosome_2b);

    Organism organism_2(gamete_2a, gamete_2b);

    if (os_) *os_ << "organism_2:\n" << organism_2 << endl;

    Organisms organisms;
    organisms.push_back(organism_1);
    organisms.push_back(organism_2);

    Population_Organisms population(organisms);

    if (os_) *os_ << "population:\n" << population << endl;

    const size_t chromosome_pair_index = 0;
    const size_t ids_per_haplotype = 1;
    PopulationSequenceMapper population_sequence_mapper(population, chromosome_pair_index, ids_per_haplotype, hapref);

    string sequence_1a = population_sequence_mapper.full_sequence(0, 1000, 1040);
    string sequence_1b = population_sequence_mapper.full_sequence(1, 1000, 1040);
    string sequence_2a = population_sequence_mapper.full_sequence(2, 1000, 1040);
    string sequence_2b = population_sequence_mapper.full_sequence(3, 1000, 1040);

    if (os_)
    {
        *os_ << "sequence_1a: " << sequence_1a << endl
             << "sequence_1b: " << sequence_1b << endl
             << "sequence_2a: " << sequence_2a << endl
             << "sequence_2b: " << sequence_2b << endl;
    }

    unit_assert(sequence_1a == sequence_1a_good);
    unit_assert(sequence_1b == sequence_1b_good);
    unit_assert(sequence_2a == sequence_2a_good);
    unit_assert(sequence_2b == sequence_2b_good);
}


int main()
{
    try
    {
        test();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

