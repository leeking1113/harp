//
// HaplotypeLikelihoodCalculator.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeLikelihoodCalculator.hpp"
#include "unit.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <iterator>
#include <cmath>


using namespace std;
using namespace boost::lambda;


namespace HaplotypeLikelihoodCalculator_Implementation
{

void get_positions_and_indices(const HaplotypeReference& hapref, const Region& read_region,
                               vector<size_t>& positions, vector<size_t>& indices_read, vector<size_t>& indices_refseq)
{
    HaplotypeReference::SNPTable::const_iterator read_begin = hapref.snp_table.lower_bound(read_region.begin);
    HaplotypeReference::SNPTable::const_iterator read_end = hapref.snp_table.lower_bound(read_region.end);
    for (HaplotypeReference::SNPTable::const_iterator it=read_begin; it!=read_end; ++it)
        positions.push_back(it->first);

    transform(positions.begin(), positions.end(), back_inserter(indices_read), _1 - read_region.begin);
    transform(positions.begin(), positions.end(), back_inserter(indices_refseq), _1 - hapref.region.begin);
}


size_t count_mismatches(string::const_iterator begin1, string::const_iterator end1, string::const_iterator begin2) 
{
    size_t mismatch_count = 0;
    for (string::const_iterator it=begin1, jt=begin2; it!=end1; ++it, ++jt)
        if (*it != *jt) ++mismatch_count;
    return mismatch_count;
}


inline bool isACGT(char base)
{
    return (base=='A' || base=='C' || base=='G' || base=='T');
}


inline bool ambiguous_base_matches(char code, char base)
{
    if (!isACGT(base)) throw runtime_error("[HaplotypeLikelihoodCalculator::ambiguous_base_matches()] base is not ACGT");

    switch(code)
    {
        case 'R': return base == 'G' || base == 'A';
        case 'Y': return base == 'T' || base == 'C';
        case 'K': return base == 'G' || base == 'T';
        case 'M': return base == 'A' || base == 'C';
        case 'S': return base == 'G' || base == 'C';
        case 'W': return base == 'A' || base == 'T';
        case 'B': return base != 'A';
        case 'D': return base != 'C';
        case 'H': return base != 'G';
        case 'V': return base != 'T';
        case 'N': return true;
        default: throw runtime_error("[HaplotypeLikelihoodCalculator::ambiguous_base_matches()] code is not IUPAC");
    }
}


class SiteLikelihoodFunctor
{
    // this function object encodes the function:
    //   haplotype_base -> log P(read_base | haplotype_base, read_quality)

    public:

    SiteLikelihoodFunctor(char read_base, char ref_base, double p_error)
    {
        if (!isACGT(ref_base)) ref_base = 'N'; // ignore ambiguous call (RYKMSWBDHV) in reference
        if (read_base == '=') read_base = ref_base;
        if (!isACGT(read_base)) read_base = 'N'; // ignore ambiguous call in read

        double logp_match = log(1-p_error);
        double logp_mismatch = log(p_error/3.);
        double logp_match_2base = log((1-p_error)/2);
        double logp_mismatch_2base = log(p_error/2);
        double logp_unknown = log(.25);

        if (isACGT(read_base))
        {
            string acgt = "ACGT";
            for (string::const_iterator it=acgt.begin(); it!=acgt.end(); ++it)
                logp_[*it] = (*it == read_base) ? logp_match : logp_mismatch;

            string rykmsw = "RYKMSW";
            for (string::const_iterator it=rykmsw.begin(); it!=rykmsw.end(); ++it)
                logp_[*it] = ambiguous_base_matches(*it, read_base) ? logp_match_2base : logp_mismatch_2base;

            logp_['N'] = logp_unknown;
        }
        else // read_base == 'N'
        {
            string codes = "ACGTRYKMSWN";
            for (string::const_iterator it=codes.begin(); it!=codes.end(); ++it)
                logp_[*it] = logp_unknown; // doesn't actually matter what value this is
        }
    }


    double operator()(char haplotype_base) const
    {
        return logp_.at(haplotype_base);
    }

    private:

    map<char,double> logp_;
};


void test_SiteLikelihoodFunctor()
{
    SiteLikelihoodFunctor f('A', 'C', 1e-4);
    const double epsilon = 1e-15;
    unit_assert_equal(f('A'), log(1 - 1e-4), epsilon);
    unit_assert_equal(f('C'), log(1e-4/3), epsilon);
    unit_assert_equal(f('G'), log(1e-4/3), epsilon);
    unit_assert_equal(f('T'), log(1e-4/3), epsilon);
    unit_assert_equal(f('Y'), log(1e-4/2), epsilon); // mismatch 2-base
    unit_assert_equal(f('R'), log((1 - 1e-4)/2), epsilon); // match 2-base
    unit_assert_equal(f('N'), log(.25), epsilon); // P(A|N) == .25

    SiteLikelihoodFunctor g('A', 'N', 1e-3);
    unit_assert_equal(g('A'), log(1 - 1e-3), epsilon);
    unit_assert_equal(g('C'), log(1e-3/3), epsilon);
    unit_assert_equal(g('G'), log(1e-3/3), epsilon);
    unit_assert_equal(g('T'), log(1e-3/3), epsilon);
    unit_assert_equal(g('Y'), log(1e-3/2), epsilon); // mismatch 2-base
    unit_assert_equal(g('R'), log((1 - 1e-3)/2), epsilon); // match 2-base
    unit_assert_equal(g('N'), log(.25), epsilon); // ref == 'N'

    SiteLikelihoodFunctor h('N', 'C', 1e-3); // read == 'N'
    unit_assert_equal(h('A'), log(.25), epsilon);
    unit_assert_equal(h('C'), log(.25), epsilon);
    unit_assert_equal(h('G'), log(.25), epsilon);
    unit_assert_equal(h('T'), log(.25), epsilon);
    unit_assert_equal(h('Y'), log(.25), epsilon);
    unit_assert_equal(h('N'), log(.25), epsilon);
}


void sum_assign(vector<double>& result, vector<double>& summand)
{
    for (vector<double>::iterator it=result.begin(), jt=summand.begin(); it!=result.end(); ++it, ++jt)
        *it += *jt;
}


} // namespace HaplotypeLikelihoodCalculator_Implementation


vector<double> HaplotypeLikelihoodCalculator::likelihood_variant(const bampp::Read& read) const
{
    using namespace HaplotypeLikelihoodCalculator_Implementation;

    // calculate positions and indices

    Region read_region(hapref_->region.id, read.position, read.position+read.sequence.size());
    vector<size_t> positions, indices_read, indices_refseq;
    get_positions_and_indices(*hapref_, read_region, positions, indices_read, indices_refseq);

    // sanity check -- verify read mapping

    const double max_mismatch_frequency = .4;
    size_t mismatch_count = count_mismatches(read.sequence.begin(), read.sequence.end(), 
                                             hapref_->refseq.begin() + read.position - hapref_->region.begin);
    if ((double(mismatch_count) / read.sequence.size()) > max_mismatch_frequency)
    {
        cout << endl << read << endl 
             << read.sequence << " read" << endl
             << hapref_->refseq.substr(read.position - hapref_->region.begin, read.sequence.size()) << " refseq" << endl
             << "mismatches: " << mismatch_count << " (" << double(mismatch_count)/read.sequence.size() << ")\n";
        //throw runtime_error("[HaplotypeLikelihoodCalculator] max_mismatch_frequency exceeded");
        cout << "[HaplotypeLikelihoodCalculator] Warning: max_mismatch_frequency exceeded\n";
    }

    // calculate likelihood vector

    string illumina_quality;
    if (illumina_base_quality_encoding_) illumina_quality = bampp::quality_to_illumina(read.quality);
    const string& quality = illumina_base_quality_encoding_ ? illumina_quality : read.quality;

    const size_t haplotype_count = hapref_->haplotype_count();
    vector<double> result(haplotype_count, 0.);
    
    for (vector<size_t>::const_iterator index_read=indices_read.begin(), 
            index_refseq=indices_refseq.begin(), position=positions.begin();
         index_read != indices_read.end(); ++index_read, ++index_refseq, ++position) // iterate over variant sites
    {
        double p_error = bqi_->error_probability(quality[*index_read]);

        // f: haplotype_base -> log P(read_base | haplotype_base, read_quality)
        SiteLikelihoodFunctor f(read.sequence[*index_read], hapref_->refseq[*index_refseq], p_error);

        // calculate likelihood vector at this variant site
        const vector<char>& haplotype_bases = hapref_->snp_table.at(*position);
        vector<double> site_likelihood(haplotype_count, 0.);
        transform(haplotype_bases.begin(), haplotype_bases.end(), site_likelihood.begin(), f);

        sum_assign(result, site_likelihood); // +=
    }

    return result;
}


vector<double> HaplotypeLikelihoodCalculator::likelihood_full(const bampp::Read& read) const
{
    using namespace HaplotypeLikelihoodCalculator_Implementation;

    // sanity check: read is in hapref region

    size_t read_length = read.sequence.size();
    if (read.position < hapref_->region.begin || read.position + read_length > hapref_->region.end)
        throw runtime_error("[HaplotypeLikelihoodCalculator::likelihood_full()] Read not contained in region.");

    // sanity check -- verify read mapping

    const double max_mismatch_frequency = .4;
    size_t mismatch_count = count_mismatches(read.sequence.begin(), read.sequence.end(), 
                                             hapref_->refseq.begin() + read.position - hapref_->region.begin);
    if ((double(mismatch_count) / read.sequence.size()) > max_mismatch_frequency)
    {
        cout << endl << read << endl 
             << read.sequence << " read" << endl
             << hapref_->refseq.substr(read.position - hapref_->region.begin, read.sequence.size()) << " refseq" << endl
             << "mismatches: " << mismatch_count << " (" << double(mismatch_count)/read.sequence.size() << ")\n";
        //throw runtime_error("[HaplotypeLikelihoodCalculator] max_mismatch_frequency exceeded");
        cout << "[HaplotypeLikelihoodCalculator] Warning: max_mismatch_frequency exceeded\n";
    }

    // calculate likelihood vector

    string illumina_quality;
    if (illumina_base_quality_encoding_) illumina_quality = bampp::quality_to_illumina(read.quality);
    const string& quality = illumina_base_quality_encoding_ ? illumina_quality : read.quality;

    const size_t haplotype_count = hapref_->haplotype_count();
    vector<double> result(haplotype_count, 0.);
    
    for (size_t index_read=0, index_refseq=read.position-hapref_->region.begin, position=read.position;
            index_read != read_length; ++index_read, ++index_refseq, ++position)
    {
        double p_error = bqi_->error_probability(quality[index_read]);

        // f: haplotype_base -> log P(read_base | haplotype_base, p_error)
        SiteLikelihoodFunctor f(read.sequence[index_read], hapref_->refseq[index_refseq], p_error);

        // calculate likelihood vector at this variant site

        vector<double> site_likelihood(haplotype_count, f(hapref_->refseq[index_refseq])); // default: monomorphic

        if (hapref_->snp_table.count(position) > 0) // variant site
        {
            const vector<char>& haplotype_bases = hapref_->snp_table.at(position);
            transform(haplotype_bases.begin(), haplotype_bases.end(), site_likelihood.begin(), f);
        } 

        sum_assign(result, site_likelihood); // +=
    }

    return result;
}


