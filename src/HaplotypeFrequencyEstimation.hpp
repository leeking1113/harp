//
// HaplotypeFrequencyEstimation.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HAPLOTYPEFREQUENCYESTIMATION_HPP_
#define _HAPLOTYPEFREQUENCYESTIMATION_HPP_


#include "HaplotypeLikelihoodRecord.hpp"
#include <vector>
#include <string>


namespace HaplotypeFrequencyEstimation {


struct Config
{
    size_t max_iteration_count;
    double convergence_threshold;
    std::string haplotype_filter;
    size_t random_start_count;
    double random_start_alpha;
    size_t random_start_seed;
    double minimum_frequency_cutoff;

    Config()
    :   max_iteration_count(30),
        convergence_threshold(0.),
        random_start_count(0), random_start_alpha(1.), random_start_seed(0),
        minimum_frequency_cutoff(1e-4)
    {}
};


std::vector<double> estimate_haplotype_frequencies(const HaplotypeLikelihoodRecords& records,
                                                   size_t position_begin,
                                                   size_t position_end,
                                                   const Config& config,
                                                   std::ostream* debug_log = 0);


} // namespace HaplotypeFrequencyEstimation


#endif // _HAPLOTYPEFREQUENCYESTIMATION_HPP_

