//
// Region.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _REGION_HPP_
#define _REGION_HPP_


#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cstdlib>


struct Region
{
    std::string id;
    size_t begin; // 0-based
    size_t end;   // iterator semantics

    // default constructor
    Region()
    :   begin(0), end(0)
    {}

    // direct constructor (0-based, iterator semantics)
    Region(const std::string& _id, unsigned int _begin, unsigned int _end)
    :   id(_id), begin(_begin), end(_end)
    {}

    // text-parsing constructor (1-based, last position inclusive)
    Region(const std::string& s) // s == chr_id:first-last (1-based)
    {
        size_t index_colon = s.find(':');
        size_t index_dash = s.find('-', index_colon);
        if (index_colon == std::string::npos || index_dash == std::string::npos) 
            throw std::runtime_error(("[Region] Bad region format: " + s).c_str());
        id = s.substr(0, index_colon);
        begin = atoi(s.substr(index_colon+1, index_dash).c_str()) - 1;
        end = atoi(s.substr(index_dash + 1).c_str());

        if (begin > end)
        {
            std::ostringstream oss;
            oss << "[Region] begin > end: " << begin << " " << end;
            throw std::runtime_error(oss.str().c_str());
        }
    }

    const std::string str() const
    {
        // convert to chr_id:first-last (1-based)
        std::ostringstream oss;
        oss << id << ":" << begin+1 << "-" << end;
        return oss.str();
    }

    const std::string filetag() const
    {
        // convert to chr_id_first-last (1-based)
        std::ostringstream oss;
        oss << id << "_" << begin+1 << "-" << end;
        return oss.str();
    }

    bool contains(size_t position) const
    {
        return position >= begin && position < end;
    }

    bool empty() const
    {
        return id.empty() && begin==0 && end==0;
    }

    Region operator&(const Region& that) const // intersection
    {
        if (this->id != that.id) return Region();
        size_t begin = std::max(this->begin, that.begin); 
        size_t end = std::min(this->end, that.end); 
        if (begin < end)
            return Region(this->id, begin, end);
        else
            return Region();
    }

    size_t length() const {return end - begin;}
};


inline std::ostream& operator<<(std::ostream& os, const Region& region)
{
    os << region.str();
    return os;
}


#endif // _REGION_HPP_
