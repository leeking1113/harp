//
// mock_02_logl_stats.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include <iostream>
#include <cmath>
#include "BAMFile.hpp"


using namespace std;
using bampp::illumina_error_probability;


extern double mock_02_qual_dist_[41][75];


double E_logl_per_site_conditional(size_t q) // E[ logl(s_i) | q_i ]
{
    if (q==0 || q==1 || q==3) return 0;
    double e = illumina_error_probability(q);
    return (1-e)*log(1-e) + e*log(e/3);
}


double E_logl2_per_site_conditional(size_t q) // E[ logl(s_i)^2 | q_i ]
{
    if (q==0 || q==1 || q==3) return 0;
    double e = illumina_error_probability(q);
    return (1-e)*log(1-e)*log(1-e) + e*log(e/3)*log(e/3);
}


inline double P(size_t q, size_t position) // P(q | position)
{
    return mock_02_qual_dist_[q][position];
}


double E_logl_per_site(size_t position)
{
    double result = 0;
    for (size_t q=0; q<41; ++q)
        result += E_logl_per_site_conditional(q) * P(q, position);
    return result;
}


double E_logl2_per_site(size_t position)
{
    double result = 0;
    for (size_t q=0; q<41; ++q)
        result += E_logl2_per_site_conditional(q) * P(q, position);
    return result;
}


double V_logl_per_site(size_t position)
{
    double mu = E_logl_per_site(position);
    return E_logl2_per_site(position) - mu*mu;
}


double E_logl()
{
    double result = 0;
    for (size_t position=0; position<75; ++position)
        result += E_logl_per_site(position);
    return result;
}


double V_logl()
{
    double result = 0;
    for (size_t position=0; position<75; ++position)
        result += V_logl_per_site(position);
    return result;
}



int main()
{
    cout << "E_logl: " << E_logl() << endl;
    double v = V_logl();
    cout << "V_logl: " << v << endl;
    cout << "sd: " << sqrt(v) << endl;
}


