//
// pair_dist_hist.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BAMFile.hpp"
#include "HaplotypeReference.hpp"
#include "harp_misc.hpp"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
using bampp::Read;


class PairDistanceHistogram : public bampp::PairProcessor
{
    public:

    virtual void process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2);
    void report();

    private:

    typedef map<int,size_t> Histogram; // distance -> count
    Histogram hist_;
};


void PairDistanceHistogram::process_pair(shared_ptr<Read> r1, shared_ptr<Read> r2)
{
    if (!r1.get() || !r2.get()) return;
    if (r1->sequence.size() != 100) return;

    int distance = int(r2->position) - int(r1->position);
    ++hist_[distance];
}


void PairDistanceHistogram::report()
{
    for (Histogram::const_iterator it=hist_.begin(); it!=hist_.end(); ++it)
        cout << it->first << " " << it->second << endl;
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 3)
        {
            cout << "pair_dist_hist\n";
            cout << "Generates a histogram of distances between read pairs from a BAM file.\n";
            cout << endl;
            cout << "Usage: pair_dist_hist bam_file region\n";
            return 1;
        }

        const char* filename_bam = argv[1];
        Region region(argv[2]);
        
        bampp::BAMFile bam(filename_bam);
        Region extended_region = harp_misc::get_extended_region(region, 200, 2000);

        PairDistanceHistogram pair_dist_hist;
        bampp::iterate_pairs(bam, region, pair_dist_hist);
        pair_dist_hist.report();
       
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

