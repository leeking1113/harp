//
// combine_mlst_hlk.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeLikelihoodRecord.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
namespace bfs = boost::filesystem;


vector<string> read_hlk_list(const string& filename)
{
    vector<string> result;
    ifstream is(filename.c_str());
    if (!is) throw runtime_error(("[combine_mlst_hlk::read_hlk_list()] Unable to open file " + filename).c_str());
    copy(istream_iterator<string>(is), istream_iterator<string>(), back_inserter(result));
    return result;
}


vector< vector<size_t> > read_st_table(const string& filename)
{
    // read in row by row

    ifstream is(filename.c_str());
    if (!is) throw runtime_error(("[combine_mlst_hlk::read_st_table()] Unable to open file " + filename).c_str());

    string buffer;
    getline(is, buffer);
    istringstream iss(buffer);
    vector<string> names;
    copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(names));
    size_t locus_count = names.size();

    vector< vector<size_t> > rows;

    while (is)
    {
        getline(is, buffer);
        if (!is) break;

        istringstream iss(buffer);
        vector<size_t> indices;
        copy(istream_iterator<size_t>(iss), istream_iterator<size_t>(), back_inserter(indices));
       
        if (indices.size() != locus_count)
            throw runtime_error("[combine_mlst_hlk::read_st_table()] Row lengths do not match.");

        rows.push_back(indices);
    }

    size_t sequence_type_count = rows.size();
    cout << "sequence types: " << sequence_type_count << endl;

    // transpose, return result as vector of columns

    vector< vector<size_t> > st_indices(locus_count);
    
    for (size_t j=0; j<locus_count; ++j)
    {
        st_indices[j].resize(rows.size());
        for (size_t i=0; i<sequence_type_count; ++i)
            st_indices[j][i] = rows[i][j];
    }

    return st_indices;
}


void process_locus(vector<size_t> indices, const string& filename_hlk, HaplotypeLikelihoodRecords& result)
{
    HaplotypeLikelihoodRecords locus_hlk(filename_hlk);
    cout << filename_hlk << " record count: " << locus_hlk.size() << endl;

    size_t st_count = indices.size();

    if (result.haplotype_count == 0)
        result.haplotype_count = st_count + 1;

    if (st_count + 1 != result.haplotype_count)
        throw runtime_error("[combine_mlst_hlk] I am insane.");

    for (HaplotypeLikelihoodRecords::const_iterator it=locus_hlk.begin(); it!=locus_hlk.end(); ++it)
    {
        HaplotypeLikelihoodRecordPtr record(new HaplotypeLikelihoodRecord);

        record->read1_begin = (*it)->read1_begin;
        record->read1_end = (*it)->read1_end;
        record->read2_begin = (*it)->read2_begin;
        record->read2_end = (*it)->read2_end;
        record->read1_logl.resize(st_count + 1); // 0/Ref
        record->read2_logl.resize(st_count + 1); // 0/Ref

        for (size_t st_index=0; st_index<st_count; ++st_index)
        {
            // note: indices[st_index] is 1-based, but so is read1_logl[] due to 0/Ref

            if (indices[st_index] >= (*it)->read1_logl.size()) throw runtime_error("bad index in hlk");
            if (indices[st_index] >= (*it)->read2_logl.size()) throw runtime_error("bad index in hlk");

            record->read1_logl[st_index + 1] = (*it)->read1_logl[indices[st_index]];
            record->read2_logl[st_index + 1] = (*it)->read2_logl[indices[st_index]];
        }

        result.push_back(record);
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 4)
        {
            cout << "Usage: combine_mlst_hlk sequence_type_table.csv hlk_list.txt output.hlk\n";
            cout << endl;
            return 1;
        }

        const char* filename_st_table = argv[1];
        const char* filename_hlk_list = argv[2];
        const char* filename_output = argv[3];

        vector< vector<size_t> > st_indices = read_st_table(filename_st_table);
        vector<string> hlk_list = read_hlk_list(filename_hlk_list);
         
        if (st_indices.size() != hlk_list.size())
            throw runtime_error("[combine_mlst_hlk] Column count in sequence type table must match hlk list size.");

        size_t locus_count = hlk_list.size();

        HaplotypeLikelihoodRecords result;

        for (size_t locus_index=0; locus_index<locus_count; ++locus_index)
        {
            process_locus(st_indices[locus_index], hlk_list[locus_index], result);
        }

        cout << "result size: " << result.size() << endl;

        ofstream os(filename_output);
        result.write(os);
        os.close();

        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

