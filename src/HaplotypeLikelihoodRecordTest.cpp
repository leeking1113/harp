//
// HaplotypeLikelihoodRecordTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeLikelihoodRecord.hpp"
#include "unit.hpp"
#include <iostream>
#include <iterator>


using namespace std;


void test_record()
{
    HaplotypeLikelihoodRecord record;

    record.read1_begin = 1;
    record.read1_end = 2;
    record.read2_begin = 3;
    record.read2_end = 4;
    double d1[] = {5., 6., 7., 8.}, d2[] = {9,10,11,12};
    copy(d1, d1+sizeof(d1)/sizeof(double), back_inserter(record.read1_logl));
    copy(d2, d2+sizeof(d2)/sizeof(double), back_inserter(record.read2_logl));

    ostringstream oss;
    record.write(oss);

    const size_t haplotype_count = 4;
    const size_t record_size = 4 * sizeof(uint32_t) + 2 * haplotype_count * sizeof(double);
    unit_assert(oss.str().size() == record_size);

    vector<uint32_t> v(4);
    istringstream iss(oss.str());
    iss.read((char*)&v[0], 4 * sizeof(uint32_t));
    vector<double> w(4);
    iss.read((char*)&w[0], 4 * sizeof(double));

    unit_assert(v[0] == 1);
    unit_assert(v[1] == 2);
    unit_assert(v[2] == 3);
    unit_assert(v[3] == 4);
    unit_assert(w[0] == 5);
    unit_assert(w[1] == 6);
    unit_assert(w[2] == 7);
    unit_assert(w[3] == 8);

    istringstream iss2(oss.str());
    HaplotypeLikelihoodRecord record2;
    record2.read(iss2, 4);

    unit_assert(record2.read1_begin == 1);
    unit_assert(record2.read1_end == 2);
    unit_assert(record2.read2_begin == 3);
    unit_assert(record2.read2_end == 4);
    unit_assert(record2.read1_logl.size() == 4);
    unit_assert(record2.read1_logl[0] == 5);
    unit_assert(record2.read1_logl[1] == 6);
    unit_assert(record2.read1_logl[2] == 7);
    unit_assert(record2.read1_logl[3] == 8);
}


void test_records()
{
    const size_t haplotype_count = 4;
    const size_t record_size = 4 * sizeof(uint32_t) + 2 * haplotype_count * sizeof(double);

    HaplotypeLikelihoodRecords records(haplotype_count);

    const size_t record_count = 3;
    for (size_t i=0; i<record_count; i++)
    {
        HaplotypeLikelihoodRecordPtr p(new HaplotypeLikelihoodRecord);
        records.push_back(p);
        p->read1_begin = i*10 + 1;
        p->read1_end = i*10 + 2;
        p->read2_begin = i*10 + 3;
        p->read2_end = i*10 + 4;

        p->read1_logl.push_back(i*10 + 5);
        p->read1_logl.push_back(i*10 + 6);
        p->read1_logl.push_back(i*10 + 7);
        p->read1_logl.push_back(i*10 + 8);

        p->read2_logl.push_back(i*20 + 5);
        p->read2_logl.push_back(i*20 + 6);
        p->read2_logl.push_back(i*20 + 7);
        p->read2_logl.push_back(i*20 + 8);
    }

    ostringstream oss;
    records.write(oss);
    const size_t header_size = 8 + 4 * sizeof(uint32_t);
    unit_assert(oss.str().size() == header_size + record_count * record_size);

    istringstream iss(oss.str());
    HaplotypeLikelihoodRecords records2;

    records2.read(iss);

    unit_assert(records2.haplotype_count == haplotype_count);
    unit_assert(records2.size() == record_count);
    for (size_t i=0; i<record_count; i++)
    {
        const HaplotypeLikelihoodRecord& record = *records2[i];
        unit_assert(record.read1_begin == i*10 + 1);
        unit_assert(record.read1_end == i*10 + 2);
        unit_assert(record.read2_begin == i*10 + 3);
        unit_assert(record.read2_end == i*10 + 4);

        unit_assert(record.read1_logl[0] == (i*10 + 5));
        unit_assert(record.read1_logl[1] == (i*10 + 6));
        unit_assert(record.read1_logl[2] == (i*10 + 7));
        unit_assert(record.read1_logl[3] == (i*10 + 8));

        unit_assert(record.read2_logl[0] == (i*20 + 5));
        unit_assert(record.read2_logl[1] == (i*20 + 6));
        unit_assert(record.read2_logl[2] == (i*20 + 7));
        unit_assert(record.read2_logl[3] == (i*20 + 8));
    }

    unit_assert(records2.max_read1_begin() == 21);
}


int main()
{
    try
    {
        test_record();
        test_records();
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

