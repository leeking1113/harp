//
// BaseQualityGeneratorTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BaseQualityGenerator.hpp"
#include "BaseQualityInterpreter.hpp"
#include "unit.hpp"
#include <iostream>
#include <numeric>
#include <iterator>


using namespace std;


double quick_mean(const vector<double>& d)
{
    const double epsilon = 1e-6;

    unit_assert(d.size() == 41);
    double sum = accumulate(d.begin(), d.end(), 0.);
    unit_assert_equal(sum, 1., epsilon);

    BaseQualityInterpreter_Illumina bqi;

    double mean = 0.;
    for (size_t i=2; i<41; ++i)
    {
        if (i != 3) 
            mean += bqi.error_probability(i) * d[i];
    }
    return mean;
}


double quick_mean(const vector< vector<double> >& distributions)
{
    unit_assert(distributions.size() == 100);
    vector<double> means;
    for (size_t i=0; i<100; ++i)
        means.push_back(quick_mean(distributions[i]));

    cout << "means:\n";
    copy(means.begin(), means.end(), ostream_iterator<double>(cout, " "));
    cout << endl;
    
    double mean = accumulate(means.begin(), means.end(), 0.)/100;
    cout << "mean: " << mean << endl;
    return mean;
}

 
void explore()
{
    //BaseQualityGenerator bqgen(.0659782); // empirical error rate

    BaseQualityGenerator bqgen(.06);
    
    const BaseQualityGenerator::QualScoreDistributions& distributions = bqgen.qual_score_distributions();

    quick_mean(distributions);
}


void test()
{
    BaseQualityGenerator bqgen(.0659782); // empirical error rate
    const BaseQualityGenerator::QualScoreDistributions& distributions = bqgen.qual_score_distributions();
    unit_assert(distributions.size() == 100); // 100 positions
    for (size_t i=0; i<100; ++i)
        unit_assert(distributions[i].size() == 41); // [0,40] qual scores
}


void test_position(BaseQualityGenerator& bqgen, size_t position)
{
    vector<size_t> hist(41); // score histogram

    const size_t draw_count = 10000;
    for (size_t i=0; i<draw_count; ++i)
        ++hist[bqgen.random_quality(position)];

    cout << "position: " << position << endl;

    for (size_t score=0; score<41; ++score)
        cout << hist[score] / float(draw_count) << " ";
    cout << endl;

    const BaseQualityGenerator::QualScoreDistributions& distributions = bqgen.qual_score_distributions();
    copy(distributions[position].begin(), distributions[position].end(), ostream_iterator<double>(cout, " "));
    cout << endl;
}


void test_random()
{
    BaseQualityGenerator bqgen(.0659782); // empirical error rate
    test_position(bqgen, 0);
    test_position(bqgen, 50);
    test_position(bqgen, 99);
}


void print_shifted_distributions()
{
    double e = .0659782; // empirical error rate

    vector<double> multipliers;
    multipliers.push_back(.5);
    multipliers.push_back(1);
    multipliers.push_back(1.5);
    multipliers.push_back(2);
    multipliers.push_back(2.5);
    multipliers.push_back(3);

    vector<size_t> positions;
    positions.push_back(0);
    positions.push_back(25);
    positions.push_back(50);
    positions.push_back(75);
    positions.push_back(99);


    for (vector<double>::const_iterator it=multipliers.begin(); it!=multipliers.end(); ++it)
    {
        BaseQualityGenerator bqgen(*it * e);
        const BaseQualityGenerator::QualScoreDistributions& q = bqgen.qual_score_distributions();

        for (vector<size_t>::const_iterator jt=positions.begin(); jt!=positions.end(); ++jt)
        {
            size_t position = *jt;
            cout << *it << " " << *it * e << " " << *jt << " ";
            copy(q[position].begin(), q[position].end(), ostream_iterator<double>(cout, " "));
            cout << endl;
        }
    }
}


int main()
{
    try
    {
        test();
        
        // not formal tests, but used for validation during development:
        //explore();
        //test_random();
        
        //print_shifted_distributions();

        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

