//
// BaseQualityInterpreter.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BaseQualityInterpreter.hpp"
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iterator>


using namespace std;


//
// Illumina uses scores in [0,40]:
//   0,1,3: unused
//   2: ('B') bad - do not use for downstream analysis
//   


namespace {
const size_t illumina_score_count_ = 42;
} // namespace


BaseQualityInterpreter_Illumina::BaseQualityInterpreter_Illumina(const string& filename)
{
    if (filename.empty())
    {
        p_error_.resize(illumina_score_count_, .75);

        for (size_t q=4; q<illumina_score_count_; ++q)
            p_error_[q] = pow(10, q/-10.);
    }
    else
    {
        ifstream is(filename.c_str());
        if (!is) throw runtime_error(("[BaseQualityInterpreter_Illumina::(constructor)] Unable to open file " + filename).c_str());
        is >> *this;
    }
}


BaseQualityInterpreter_Illumina::BaseQualityInterpreter_Illumina(const vector<double>& p_error)
:   p_error_(p_error)
{
    if (p_error_.size() != illumina_score_count_)
        throw runtime_error("[BaseQualityInterpreter_Illumina::(vector constructor)] Invalid vector size.");
}


bool BaseQualityInterpreter_Illumina::is_valid(char q) const
{
    return q>=0 && q<char(illumina_score_count_);
}


double BaseQualityInterpreter_Illumina::error_probability(char q) const
{
    if (!is_valid(q))
        throw runtime_error("[BaseQualityInterpreter_Illumina::error_probability()] Invalid Illumina quality score.");
    return p_error_[q];
}


ostream& operator<<(ostream& os, const BaseQualityInterpreter_Illumina& bqi)
{
    os << "BaseQualityInterpreter_Illumina ";
    copy(bqi.p_error_.begin(), bqi.p_error_.end(), ostream_iterator<double>(os, " "));
    os << endl;
    return os;
}


istream& operator>>(istream& is, BaseQualityInterpreter_Illumina& bqi)
{
    string name;
    is >> name;
    if (name != "BaseQualityInterpreter_Illumina")
        throw runtime_error("[BaseQualityInterpreter_Illumina::operator>>] Invalid input.");
    bqi.p_error_.clear();
    copy(istream_iterator<double>(is), istream_iterator<double>(), back_inserter(bqi.p_error_));
    if (bqi.p_error_.size() != illumina_score_count_)
        throw runtime_error("[BaseQualityInterpreter_Illumina::operator>>] Invalid vector size.");
    return is;
}


