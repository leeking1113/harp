//
// HaplotypeReference.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HAPLOTYPEREFERENCE_HPP_
#define _HAPLOTYPEREFERENCE_HPP_


#include "Region.hpp"
#include "boost/iostreams/positioning.hpp"
#include <tr1/memory>
#include <vector>
#include <string>
#include <map>


// cached data for reference and variants in a single chromosomal window
struct HaplotypeReference
{
    Region region;
    std::vector<std::string> haplotype_ids;

    typedef std::map<size_t, std::vector<char> > SNPTable; // 0-based position -> vector of variants
    SNPTable snp_table; 

    std::string refseq;

    HaplotypeReference(const std::string& filename_refseq, 
                       const std::string& filename_snps, 
                       const Region& _region);

    size_t haplotype_count() const {return haplotype_ids.size();}

    std::string variant_sequence(size_t haplotype_index, std::vector<size_t> positions) const;
    std::string full_sequence(size_t haplotype_index, size_t position_begin, size_t position_end,
                              bool use_ref_for_ambiguous = false) const;

    void append_recombined_haplotype(size_t haplotype_index_1, size_t haplotype_index_2, size_t recombination_position);

    // current: uses DRGP sparse SNP format (1-based positions)
    // future: use verbose output: probability vector (A,C,G,T) for each position
};


typedef std::map<size_t, boost::iostreams::stream_offset> SNPTableIndex; // 0-based position -> file offset
typedef std::tr1::shared_ptr<SNPTableIndex> SNPTableIndexPtr;
SNPTableIndexPtr create_snp_table_index(const std::string& filename_snps, size_t lines_per_entry);
void write_snp_table_index(SNPTableIndexPtr index, const std::string& filename);


#endif // _HAPLOTYPEREFERENCE_HPP_
