//
// qual_hist_fastq_raw.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>


using namespace std;


void process_file(const string& filename)
{
    cerr << "Processing file " << filename << endl;

    ifstream is(filename.c_str());
    if (!is) throw runtime_error(filename + " not found.");

    size_t read_count = 0;
    size_t base_count = 0;
    vector<size_t> counts(256);
    size_t min_score = 255;
    size_t max_score = 0;

    while (is)
    {
        string id_1, sequence, id_2, quality;
        getline(is, id_1);
        getline(is, sequence);
        getline(is, id_2);
        getline(is, quality);
        if (!is) break;

        ++read_count;
        base_count += quality.size();

        for (string::const_iterator it=quality.begin(); it!=quality.end(); ++it)
        {
            size_t score = static_cast<size_t>(*it);
            ++counts[score];

            if (score < min_score) min_score = score;
            if (score > max_score) max_score = score;
        }
    }

    // report stuff

    cout << "# reads " << read_count << endl;
    cout << "# bases " << base_count << endl;
    cout << "# min score " << min_score << endl;
    cout << "# max score " << max_score << endl;

    for (size_t i=0; i<256; ++i)
        cout << i << " " << counts[i] << endl;
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            cout << "qual_hist_fastq_raw\n";
            cout << "Prints raw base quality score distribution from fastq file.\n";
            cout << endl;
            cout << "Usage: qual_hist_fastq_raw filename\n";
            cout << endl;
            return 1;
        }

        const char* filename = argv[1];
        process_file(filename);
          
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

