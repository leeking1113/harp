#!/bin/bash

git archive --prefix=harp/ master | bzip2 > harp.tbz
scp harp.tbz dkessner@hoffman2.idre.ucla.edu:~/dev

