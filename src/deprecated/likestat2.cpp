//
// likestat2.cpp
//
// Darren Kessner
// Novembre Lab, UCLA
//


#include "HaplotypeLikelihoodRecord.hpp"
#include "HaplotypeFrequencyEstimation.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <iterator>
#include <cmath>
#include <cstdlib>


using namespace std;


double calculate_diff2(const vector<double>& f1, const vector<double>& f2)
{
    if (f1.size() != f2.size()) throw runtime_error("[likestat2] This isn't happening.");

    double result = 0.;
    for (vector<double>::const_iterator it1=f1.begin(), it2=f2.begin(); it1!=f1.end(); ++it1, ++it2)
    {
        double diff = *it1 - *it2;
        result += (diff*diff);
    }
    return result;
}


void go(const HaplotypeLikelihoodRecords& records1, const HaplotypeLikelihoodRecords& records2, 
        size_t position_begin, size_t step)
{
    if (records1.empty()) throw runtime_error("[likestat] No records1.");
    if (records2.empty()) throw runtime_error("[likestat] No records2.");

    size_t position_end = max(records1.back()->read1_begin, records2.back()->read1_begin) + 1;

    for (size_t position=position_begin; position<position_end; position+=step)
    {
        using HaplotypeFrequencyEstimation::estimate_haplotype_frequencies;
        vector<double> f1 = estimate_haplotype_frequencies(records1, position, position+step, 1);
        vector<double> f2 = estimate_haplotype_frequencies(records2, position, position+step, 1);

        double diff2 = -1.;

        if (!f1.empty() && !f2.empty())
            diff2 = calculate_diff2(f1, f2);

        cout << position << "\t" << position+step << "\t" << diff2 << endl;
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 5)
        {
            cout << "Usage: likestat2 file1.hlk file2.hlk position_begin step\n";
            return 1;
        }

        const char* filename1 = argv[1];
        const char* filename2 = argv[2];
        size_t position_begin = atoi(argv[3]);
        size_t step = atoi(argv[4]);

        HaplotypeLikelihoodRecords records1(filename1);
        HaplotypeLikelihoodRecords records2(filename2);
        go(records1, records2, position_begin, step);

        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


