//
// harp_config.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"
#include <iostream>
#include <fstream>


using namespace std;
namespace po = boost::program_options;
namespace bfs = boost::filesystem;


namespace harp_config {


string options_string_;


Config::Config(int argc, char* argv[])
:   verbose(false), compute_standard_errors(false), 
    min_mapping_quality(15),
    illumina_base_quality_encoding(false),
    logl_min_zscore(-numeric_limits<double>::infinity()),
    logl_mean(0), logl_sd(1),
    window_step_size(0), window_width(0), 
    max_mismatches(3)
{
    // command line only options (not in config file)

    string filename_config;

    po::options_description od_commandline_only("Command line only");
    od_commandline_only.add_options()
        ("config,c", po::value<string>(&filename_config)->default_value("config.harp"), "filename of configuration file")
        ;

    // general configuration options
    
    string region_string;

    po::options_description od_input("Input");
    od_input.add_options()
        ("bam,b", po::value<string>(&filename_bam), "BAM filename")
        ("bamlist", po::value<string>(&filename_bamlist), "text file list of BAM files")
        ("region,r", po::value<string>(&region_string), "region (e.g. 2L:1001-2000)")
        ("refseq", po::value<string>(&filename_refseq), "refseq filename")
        ("refseqlist", po::value<string>(&filename_refseqlist), "text file list of refseq filenames")
        ("snps", po::value<string>(&filename_snps), "SNP filename")
        ;

    po::options_description od_output("Output");
    od_output.add_options()
        ("stem", po::value<string>(&filename_stem), "output filename stem [default: generated from BAM filename and region]")
        ("out", po::value<string>(&dirname_output), "directory for additional output files [default: (stem).output]")
        ("hlk", po::value<string>(&filename_hlk), "haplotype likelihood filename (.hlk) [default: (stem).hlk]")
        ("freqs", po::value<string>(&filename_freqs), "haplotype frequencies filename (.freqs) [default: (stem).freqs]")
        ("verbose,v", "verbose output")
        ("compute_standard_errors", "compute standard errors")
        ;

    po::options_description od_like("Likelihood calculation (like)");
    od_like.add_options()
        ("bqi", po::value<string>(&filename_bqi), "base quality interpretation filename")
        ("min_mapping_quality", po::value<int>(&min_mapping_quality)->default_value(min_mapping_quality), "minimum mapping quality")
        ("illumina_base_quality_encoding,I", "Illumina encoding of base quality scores [0,40] as ASCII [64,104]")
        ;

    po::options_description od_like_multi("Likelihood multiple-reference calculation (like_multi)");
    od_like_multi.add_options()
        ("logl_min_zscore", po::value<double>(&logl_min_zscore)->default_value(logl_min_zscore), "filter out reads below specified minimum log-likelihood")
        ("logl_mean", po::value<double>(&logl_mean)->default_value(logl_mean), "required with logl_min_zscore")
        ("logl_sd", po::value<double>(&logl_sd)->default_value(logl_sd), "required with logl_min_zscore")
        ;

    po::options_description od_freq("Frequency estimation (freq)");
    od_freq.add_options()
        ("window_step", po::value<size_t>(&window_step_size), "window step size [default: length of region, i.e. single window]")
        ("window_width", po::value<size_t>(&window_width), "window width [default: window_step]")
        ("em_iter", po::value<size_t>(&freq_config.max_iteration_count)->default_value(freq_config.max_iteration_count), "EM iteration count")
        ("em_converge", po::value<double>(&freq_config.convergence_threshold)->default_value(freq_config.convergence_threshold), "EM convergence threshold")
        ("em_random_start_count", po::value<size_t>(&freq_config.random_start_count)->default_value(freq_config.random_start_count), "number of additional random starts")
        ("em_random_start_alpha", po::value<double>(&freq_config.random_start_alpha)->default_value(freq_config.random_start_alpha), "symmetric dirichlet parameter for random initial estimates")
        ("em_random_start_seed", po::value<size_t>(&freq_config.random_start_seed)->default_value(freq_config.random_start_seed), "seed for random initial estimates")
        ("em_min_freq_cutoff", po::value<double>(&freq_config.minimum_frequency_cutoff)->default_value(freq_config.minimum_frequency_cutoff), "EM minimum frequency cutoff")
        ("haplotype_filter", po::value<string>(&freq_config.haplotype_filter), "haplotype filter (0 == Ref) [e.g. 1-3,5-7,10]")
        ;

    po::options_description od_freq_stringmatch("String matching requency estimation (freq_stringmatch)");
    od_freq_stringmatch.add_options()
        ("max_mismatch", po::value<size_t>(&max_mismatches)->default_value(max_mismatches), "maximum # of mismatches allowed")
        ;

    po::options_description od_hidden("Hidden options");
    od_hidden.add_options()
        ("function", po::value< vector<string> >(&function), "function")
        ;

    po::options_description od_all;
    od_all.add(od_commandline_only).add(od_input).add(od_output).add(od_like).add(od_like_multi).add(od_freq).add(od_freq_stringmatch).add(od_hidden);

    po::positional_options_description p;
    p.add("function", -1);

    // store the options string

    if (options_string_.empty())
    {
        po::options_description od_options_string;
        od_options_string.add(od_commandline_only).add(od_input).add(od_output).add(od_like).add(od_like_multi).add(od_freq).add(od_freq_stringmatch);

        ostringstream oss;
        oss << od_options_string;

        options_string_ = oss.str();
    }

    if (argc == 0 && argv == 0) return;

    // parse command line
    
    po::variables_map vm;
    store(po::command_line_parser(argc, argv).
          options(od_all).positional(p).run(), vm);
    notify(vm);

    // parse config file
    
    ifstream is(filename_config.c_str());
    if (is)
    {
        cout << "Parsing config file " << filename_config << endl;
        store(parse_config_file(is, od_all), vm);
        notify(vm);
    }

    // post-parse processing
    
    if (!region_string.empty())
        region = Region(region_string);

    if (filename_stem.empty() && !filename_bam.empty() && !region_string.empty())
        filename_stem = filename_stem_from_bam();

    if (filename_stem.empty() && !filename_hlk.empty()) // special case: use .hlk stem if no BAM specified
        filename_stem = bfs::path(filename_hlk).stem().string();

    if (dirname_output.empty() && !filename_stem.empty())
        dirname_output = filename_stem + ".output";

    if (filename_hlk.empty() && !filename_stem.empty())
        filename_hlk = filename_stem + ".hlk";

    if (filename_freqs.empty() && !filename_stem.empty())
        filename_freqs = filename_stem + ".freqs";

    if (vm.count("verbose")) verbose = true;
    if (vm.count("compute_standard_errors")) compute_standard_errors = true;
    if (vm.count("illumina_base_quality_encoding")) illumina_base_quality_encoding = true;

    if (window_step_size == 0) window_step_size = region.length();
    if (window_width == 0) window_width = window_step_size;

    if (logl_min_zscore > -numeric_limits<double>::infinity() &&
        logl_mean == 0 && logl_sd == 1)
        throw runtime_error("[harp_config] log_mean and logl_sd must be set if logl_min_zscore is used"); 
}


std::string Config::filename_stem_from_bam() const
{
    ostringstream oss;
    oss << bfs::path(filename_bam).stem().string() << "." 
        << region.id << "_" << region.begin+1 << '-' << region.end;
    return oss.str();
}


ostream& operator<<(ostream& os, const Config& config)
{
    // careful: for flags like verbose, em_random_start:
    //   "flag = 0" doesn't work properly in config file -- must delete line "flag = 1"

    for (vector<string>::const_iterator it=config.function.begin(); it!=config.function.end(); ++it)
        os << "function = " << *it << endl;
    os << "bam = " << config.filename_bam << endl;
    os << "bamlist = " << config.filename_bamlist << endl;
    os << "region = " << config.region << endl;
    os << "refseq = " << config.filename_refseq << endl;
    os << "refseqlist = " << config.filename_refseqlist << endl;
    os << "snps = " << config.filename_snps << endl;

    os << "stem = " << config.filename_stem << endl;
    os << "out = " << config.dirname_output << endl;
    os << "hlk = " << config.filename_hlk << endl;
    os << "freqs = " << config.filename_freqs << endl;
    if (config.verbose) os << "verbose = 1\n";
    if (config.compute_standard_errors) os << "compute_standard_errors = 1\n";

    os << "filename_bqi = " << config.filename_bqi << endl;
    os << "min_mapping_quality = " << config.min_mapping_quality << endl;
    if (config.illumina_base_quality_encoding) os << "illumina_base_quality_encoding = 1\n";

    if (config.logl_min_zscore > -numeric_limits<double>::infinity())
    {
        os << "logl_min_zscore = " << config.logl_min_zscore << endl;
        os << "logl_mean = " << config.logl_mean << endl;
        os << "logl_sd = " << config.logl_sd << endl;
    }

    os << "window_step = " << config.window_step_size << endl;
    os << "window_width = " << config.window_width << endl;
    os << "haplotype_filter = " << config.freq_config.haplotype_filter << endl;
    os << "em_iter = " << config.freq_config.max_iteration_count << endl;
    os << "em_converge = " << config.freq_config.convergence_threshold << endl;
    os << "em_random_start_count = " << config.freq_config.random_start_count << endl;
    os << "em_min_freq_cutoff = " << config.freq_config.minimum_frequency_cutoff << endl;

    return os;
}


const string& options_string()
{
    Config config; // hack to create the string
    return options_string_;
}


} // namespace harp_config


