//
// pileup_to_hippo.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "HaplotypeReference.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;
namespace bfs = boost::filesystem;


double alt_fraction(char ref, const string& pileup)
{
    size_t ref_count = 0;
    size_t alt_count = 0;
    const char* begin = pileup.c_str();

    for (const char* p=begin; p<begin+pileup.size(); ++p)
    {
        char c = toupper(*p);

        if (c == ref)
            ++ref_count;
        else if (c == '$')
            continue; 
        else if (c == '^')
        {
            ++p; // advance past quality char
            continue; 
        }
        else if (c == '+' || c == '-')
        {
            ++p; // skip past +/-
            size_t n = atoi(p);
            size_t digits = size_t(log10(n)) + 1;
            p += digits + n - 1; // for loop gives us an extra increment
        }
        else if (c=='*')
            continue; // known unknown
        else
            ++alt_count;
    }

    return double(alt_count) / (ref_count + alt_count);
}


struct Config
{
    string filename_snps;
    string filename_stem;
    Region region;
    size_t haplotype_count;
    size_t poolsize;

    Config(const char* filename)
    :   haplotype_count(0), poolsize(0)
    {
        ifstream is(filename);
        for (string buffer; getline(is, buffer);)
        {
            istringstream iss(buffer);
            string name, value;
            iss >> name >> value;
            if (name == "filename_snps") filename_snps = value;
            else if (name == "filename_stem") filename_stem = value;
            else if (name == "region") region = Region(value);
            else if (name == "haplotype_count") haplotype_count = atoi(value.c_str());
            else if (name == "poolsize") poolsize = atoi(value.c_str());
            else throw runtime_error("Unknown parameter name: " + name);
        }
    }
};


int main(int argc, char* argv[])
{
    try
    {
        if (argc < 3)
        {
            cout << "Usage: pileup_to_hippo filename.config filename.pileup outdir\n";
            return 1;
        }

        const char* filename_config= argv[1];
        bfs::path filename_pileup(argv[2]);
        bfs::path outdir(argv[3]);

        Config config(filename_config);
        shared_ptr<HaplotypeReference> hapref(new HaplotypeReference("", config.filename_snps, config.region));

        if (config.filename_stem.empty()) throw runtime_error("Empty filename_stem.");
        if (config.haplotype_count >= hapref->haplotype_count()) throw runtime_error("haplotype_count too high");

        bfs::create_directories(outdir);

        // .hippo_data
       
        bfs::ifstream is(filename_pileup);
        map<size_t, size_t> counts;

        for (string buffer; getline(is, buffer);) // iterate over lines in pileup (loci)
        {
            istringstream iss(buffer);
            vector<string> tokens;
            copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(tokens));
            size_t position = atoi(tokens[1].c_str()) - 1; // 0-based
            const string& pileup = tokens[4];

            if (hapref->snp_table.count(position) == 0) continue;
            
            double f = alt_fraction(hapref->snp_table[position][0], pileup);
            size_t count = size_t(round(f * config.poolsize * 2)); // 2 == diploid allele counts
            counts[position] = count;
        }

        bfs::path filename_data(config.filename_stem + ".hippo_data");
        cout << filename_data.string() << endl;
        bfs::ofstream os_data(outdir / filename_data);
        os_data << config.poolsize << " ";

        for (HaplotypeReference::SNPTable::const_iterator it=hapref->snp_table.begin(); it!=hapref->snp_table.end(); ++it)
        {
            size_t position = it->first;
            os_data << counts[position] << " ";
        }

        os_data << endl;
        os_data.close();

        // .hippo_hap

        bfs::path filename_hap(config.filename_stem + ".hippo_hap");
        cout << filename_hap.string() << endl;
        bfs::ofstream os_hap(outdir / filename_hap);
        
        os_hap << config.haplotype_count << endl;

        for (size_t h=1; h<=config.haplotype_count; ++h) 
        {
            for (HaplotypeReference::SNPTable::const_iterator it=hapref->snp_table.begin(); it!=hapref->snp_table.end(); ++it)
            {
                    os_hap << (it->second[h] != it->second[0]) << " ";
            }
            os_hap << endl;
        }

        os_hap.close();

        // .hippo_par

        bfs::path filename_par(config.filename_stem + ".hippo_par");
        cout << filename_par.string() << endl;
        bfs::ofstream os_par(outdir / filename_par);

        os_par << "data_file " << filename_data.string() << endl
            << "hap_file " << filename_hap.string() << endl
            << "n_loci " << hapref->snp_table.size() << endl
            << "n_pools 1\n"
            << "n_iterations 500000\n"
            << "n_burnin 50000\n"
            << "variable_list 0\n"
            << "gamma 12\n"
            << "alpha 0.000001\n"
            << "p_add 0.5\n"
            << "c_old 10.0\n"
            << "c_new 2.0\n"
            << "c 10.0\n"
            << "d 0.2\n"
            << "tol 0.0\n";
        os_par.close();

        // .aeml_par

        bfs::path filename_aeml_par(config.filename_stem + ".aeml_par");
        cout << filename_aeml_par.string() << endl;
        bfs::ofstream os_aeml_par(outdir / filename_aeml_par);

        os_aeml_par << "data_file " << filename_data.string() << endl
            << "hap_file " << filename_hap.string() << endl
            << "n_loci " << hapref->snp_table.size() << endl
            << "n_pools 1\n"
            << "tol 0.0\n";
        os_aeml_par.close();
      
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

