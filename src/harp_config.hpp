//
// harp_config.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _HARP_CONFIG_
#define _HARP_CONFIG_


#include "Region.hpp"
#include "HaplotypeFrequencyEstimation.hpp"
#include <string>
#include <vector>
#include <iosfwd>
#include <map>


namespace harp_config {


struct Config
{
    std::vector<std::string> function;

    // input

    std::string filename_bam;
    std::string filename_bamlist;
    Region region;
    std::string filename_refseq;
    std::string filename_refseqlist;
    std::string filename_snps;

    // output

    std::string filename_stem; // default: filename_stem_from_bam()
    std::string dirname_output; // default: filename_stem + ".output"
    std::string filename_hlk; // default: filename_stem + ".hlk"
    std::string filename_freqs; // default: filename_stem + ".freqs"
    bool verbose;
    bool compute_standard_errors;

    // like-specific

    std::string filename_bqi;
    int min_mapping_quality;
    bool illumina_base_quality_encoding; // [0,40] encoded as ASCII [64,104]

    // like_multi-specific

    double logl_min_zscore;
    double logl_mean;
    double logl_sd;

    // freq-specific

    size_t window_step_size;
    size_t window_width;
    HaplotypeFrequencyEstimation::Config freq_config;

    // freq_stringmatch-specific

    size_t max_mismatches;


    Config(int argc = 0, char* argv[] = 0);

    std::string filename_stem_from_bam() const; // returns stem(filename_bam).region
};


std::ostream& operator<<(std::ostream& os, const Config& config);


const std::string& options_string();


// jump table for harp functions


typedef int (*Function)(const harp_config::Config& config);
typedef std::string (*Info)();
typedef std::string (*Usage)();


struct JumpTableEntry
{
    Function function;
    Info info;
    Usage usage;

    JumpTableEntry(Function f=0, Info i=0, Usage u=0)
    :  function(f), info(i), usage(u) 
    {}
};


typedef std::map<std::string, JumpTableEntry> JumpTable;


} // namespace harp_config


#endif // _HARP_CONFIG_


