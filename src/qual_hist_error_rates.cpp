//
// qual_hist_error_rates.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BaseQualityGenerator.hpp"
#include "BaseQualityInterpreter.hpp"
#include "PiecewiseUniformDistribution.hpp"
#include <iostream>
#include <iterator>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <numeric>
#include <cmath>
#include <tr1/memory>


using namespace std;
using tr1::shared_ptr;


static const size_t position_count_ = 100; // hard-coded: 100 positions
static const size_t score_count_ = 41; // Illumina qual scores in [0,40]


typedef vector< vector<double> > Dists; // vector<double>(score_count_) for each position


Dists read_file(const char* filename)
{
    Dists dists(position_count_);
    for (Dists::iterator it=dists.begin(); it!=dists.end(); ++it)
        it->resize(score_count_);

    ifstream is(filename);
    if (!is) throw runtime_error("Unable to open file.");

    size_t score = 0;

    while (is)
    {
        string buffer;
        getline(is, buffer);
        if (!is) break;
        if (buffer[0] == '#') continue;

        istringstream iss(buffer);

        for (size_t position=0; position<position_count_; ++position)
            iss >> dists[position][score];

        ++score;
    }
    
    return dists;
}


void compute_error_rates(const char* filename)
{
    Dists dists = read_file(filename);

    double sum_means = 0;

    vector<double> x;
    x.push_back(0);

    BaseQualityInterpreter_Illumina bqi;

    for (char q=40; q>3; --q) x.push_back(bqi.error_probability(q));
    x.push_back(bqi.error_probability(2));

    cout << "error rates per position: ";

    for (size_t position=0; position<position_count_; ++position)
    {
        const vector<double>& dist = dists[position];
        double sum_dist = accumulate(dist.begin(), dist.end(), 0.);

        vector<double> p;
        p.push_back(0);
        for (size_t q=40; q>3; --q) p.push_back(dist[q]/sum_dist);
        p.push_back(dist[2]/sum_dist);

        PiecewiseUniformDistribution pud(x, p);
        double mean = pud.mean();

        if (!isnan(mean)) sum_means += mean;
        cout << mean << " ";
    }

    cout << endl;
    cout << "total error rate: " << sum_means << endl;
    cout << "mean error rate per position: " << sum_means/position_count_ << endl;
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            cout << "qual_hist_error_rates\n";
            cout << "Calculates error rates from base quality score histogram.";
            cout << endl;
            cout << "Usage: qual_hist_error_rates filename.qh\n";
            return 1;
        }

        const char* filename = argv[1];
        compute_error_rates(filename);
        return 0;
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}

