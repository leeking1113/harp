#!/usr/bin/env RScript
#
# plot_qh.R
#
# Darren Kessner
# Novembre Lab, UCLA
#


plot_by_position = function(m)
{
    plot(m[,1], type='l')

    positions = seq(15, 100, 20)
    line_colors = 1:length(positions)

    for (i in 1:length(positions))
    {
        lines(m[,positions[i]], col=line_colors[i])
    }

    legend(10, .25, positions, col=line_colors, lty=1)
}



plot_B = function(m)
{
    plot(m[3,])
}



main = function()
{
    args = commandArgs(TRUE)
    if (length(args) != 1)
    {
        cat("Usage: plot_qh.R filename\n")
        q(status=1)
    }

    filename = args[1]

    m_unnormalized = as.matrix(read.table(filename))
    read_count = sum(m_unnormalized[,1])
    m = apply(m_unnormalized, 2, function(x){x/read_count})

    plot_by_position(m)

    #plot_B(m2)
}

main()

