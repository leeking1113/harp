//
// harp.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "harp_config.hpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <iostream>
#include <map>
#include <string>
#include <stdexcept>


using namespace std;
namespace bfs = boost::filesystem;


harp_config::JumpTable jump_table_;


void create_jump_table()
{
    extern harp_config::JumpTableEntry entry_harp_like_;
    jump_table_["like"] = entry_harp_like_;

    extern harp_config::JumpTableEntry entry_harp_like_multi_;
    jump_table_["like_multi"] = entry_harp_like_multi_;

    extern harp_config::JumpTableEntry entry_harp_likedump_;
    jump_table_["likedump"] = entry_harp_likedump_;

    extern harp_config::JumpTableEntry entry_harp_freq_;
    jump_table_["freq"] = entry_harp_freq_;

    extern harp_config::JumpTableEntry entry_harp_freq_stringmatch_;
    jump_table_["freq_stringmatch"] = entry_harp_freq_stringmatch_;

    extern harp_config::JumpTableEntry entry_harp_bqi_;
    jump_table_["bqi"] = entry_harp_bqi_;

/*
    extern harp_config::JumpTableEntry entry_harp_recomb_;
    jump_table_["recomb"] = entry_harp_recomb_;

    int harp_allele_counts(const harp_config::Config& config);
    jump_table_["allele_counts"] = harp_allele_counts;
    jump_table_info_["allele_counts"] = "report allele counts (increased/decreased/start)";
*/
}


int main(int argc, char* argv[])
{
    try
    {
        create_jump_table();

        if (argc < 2)
        {
            cout << "harp: Haplotype Analysis of Reads in Pools\n";
            cout << endl;
            cout << "Usage: harp function_name [args]\n";
            cout << endl;
            cout << "Available functions:\n";
            for (harp_config::JumpTable::const_iterator it=jump_table_.begin(); it!=jump_table_.end(); ++it)
                cout << "  " << it->first << " : " << jump_table_[it->first].info() << endl;
            cout << endl;
            cout << "For required parameters, use 'harp function_name' with no arguments.\n";
            cout << endl;
            cout << "Parameters may be specified on the command line:\n";
            cout << "  --name value    (e.g. --bam filename.bam)\n";
            cout << "or in a configuration file:\n";
            cout << "  name = value    (e.g. bam = filename.bam)\n";
            cout << endl;
            cout << "Parameters:\n"; 
            cout << harp_config::options_string() << endl;
            cout << endl;
            cout << "Darren Kessner, Novembre Lab UCLA (dkessner@ucla.edu)\n";
            return 1;
        }

        if (argc == 2)
        {
            const char* function_name = argv[1];
            if (jump_table_.count(function_name))
                cout << jump_table_[function_name].usage() << endl;
            else
                cout << "Unknown function: " << function_name << endl;
            return 1;
        }

        harp_config::Config config(argc, argv);
        if (config.verbose) cout << config << endl;

        if (config.function.size() != 1)
        {
            ostringstream oss;
            oss << "[harp] Exactly one function must be specified.\n";
            oss << config.function.size() << " functions were specified on the command line: ";
            copy(config.function.begin(), config.function.end(), ostream_iterator<string>(oss, " "));
            throw runtime_error(oss.str().c_str());
        }

        const string& function_name = config.function[0];

        if (jump_table_.count(function_name) == 0)
            throw runtime_error(("[harp] Unknown function: " + function_name).c_str());

        jump_table_[function_name].function(config);
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


